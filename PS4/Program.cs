﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS4
{
    class Program
    {
        static void Main(string[] args)
        {
            PS4 ps4 = new PS4();
            ps4.ObliczPrawdopodobienstwaStanow();
            ps4.PrawdopodobienstwoStratyRuchuTranzyt();
            ps4.PrawdopodobienstwoStratyRuchuLokal();
            ps4.WspolczynnikStratRuchuTranzyt();
            ps4.WspolczynnikStratRuchuLokal();
            ps4.CalkowitaStrata();

          //  ps4.ZrzutDoCsv();
        }
    }
}
