﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace PS4
{
    class PS4
    {
        int c = 3;
        int m1 = 6;
        int m = 10;
        double mi = 10.0;
        List<double> lambda1 = new List<double>();
        List<double> lambda2 = new List<double>();

        //Zad1
        List<double> zad1 = new List<double>();
        //Zad2
        List<double> zad2 = new List<double>();
        List<double> alt2 = new List<double>();
        //Zad3
        List<double> zad3 = new List<double>();
        //Zad4
        List<double> zad4 = new List<double>();
        //Zad5
        List<double> zad5 = new List<double>();
        //Zad6
        List<double> zad6 = new List<double>();

        public PS4()
        {
            for (int i = 2; i <= 14; i+=2)
            {
                lambda1.Add(i);
                lambda2.Add(2*i);
            }

            List<double> wynikiQ = new List<double>();

            for (int i = 0; i <= m+c; i++)
            {
                wynikiQ.Add(ObliczQ(i, 10, 20));
            }


        }

        int Silnia(int liczba)
        {
            int wynik = 1;

            for (int i = 1; i <= liczba; i++)
            {
                wynik *= i;
            }

            return wynik;
        }

        double ObliczQ(int numer, double l1, double l2)
        {
            double ro; //= (l1 + l2)/mi;
            double wynik;
            
            if (numer <= c-1)
            {
                ro = (l1 + l2) /( (numer+1) * mi);
                wynik = Math.Pow(ro,numer)/Silnia(numer);
                return wynik;
            }
            else if(numer >= c && numer <=(c+m1-1))
            {
                ro = (l1 + l2) / (c * mi);
                wynik = (Math.Pow(ro, c) / Silnia(c)) * Math.Pow(ro / c, numer - c);
                return wynik;
            }
            else
            {
                ro = (l1 + l2) / (c * mi);
                double ro2 = l2 / (c * mi);
                wynik = (Math.Pow(ro, c) / Silnia(c)) * Math.Pow(ro / c, m1) * Math.Pow(ro2 / c, numer - (c + m1));
                return wynik;
            }
        }

        //Zad1
        public void ObliczPrawdopodobienstwaStanow()
        {

            //oblicz p0;
            //
            double sumaQ = 0;
            for (int i = 0; i <= m+c; i++)
            {
                sumaQ += ObliczQ(i, 10, 20);
            }
            double p0 = 1 / sumaQ;

            zad1.Add(p0);

            for (int i = 1; i <= m+c; i++)
            {
                double q = ObliczQ(i, 10, 20);
                zad1.Add(p0 * q);
            }

            Console.WriteLine("Obliczono prawdopodobienstwa stanow.");
        }
        //Zad2
        public void PrawdopodobienstwoStratyRuchuTranzyt()
        {
            for (int i = 0; i < 7; i++)
            {
                //oblicz p0;
                //
                double sumaQ = 0;
                for (int j = 0; j <= m + c;j++)
                {
                    sumaQ += ObliczQ(j, lambda1[i], lambda2[i]);
                }
                double p0 = 1 / sumaQ;
                //p0 policzone
            
                double ro = (lambda1[i] + lambda2[i]) / ( c *mi);
                double ro2 = lambda2[i] / (c * mi);
                double wynik = p0 * (Math.Pow(ro, c) / Silnia(c)) * Math.Pow(ro / c, m1) * Math.Pow(ro2 / c, m - m1);
                zad2.Add(wynik);

                alt2.Add(p0 * ObliczQ(m + c, lambda1[i], lambda2[i]));

            }
            Console.WriteLine("Obliczono prawdopodobienstwo straty ruchu tranzytowego.");
        }
        //Zad3
        public void PrawdopodobienstwoStratyRuchuLokal()
        {
            for (int i = 0; i < 7; i++)
            {
                //oblicz p0;
                //
                double sumaQ = 0;
                for (int j = 0; j <= m + c; j++)
                {
                    sumaQ += ObliczQ(j, lambda1[i], lambda2[i]);
                }
                double p0 = 1 / sumaQ;
                //p0 policzone

                //suma
                double suma = 0;

                double ro = (lambda1[i] + lambda2[i]) / (mi);
                double ro2 = lambda2[i] / (mi);

                for (int k= 0; k<= m - m1; k++)
                {
                    suma += Math.Pow(ro2 / c, k);
                }

                //double 
                double wynik = p0 * (Math.Pow(ro, c) / Silnia(c)) * Math.Pow(ro / c, m1) * suma;
                zad3.Add(wynik);

            }
            Console.WriteLine("Obliczono prawdopodobienstwo straty ruchu lokalnego.");
        }
        //Zad4
        public void WspolczynnikStratRuchuTranzyt()
        {
            for(int i = 0; i < 7; i++)
            {
                double ro = (lambda1[i] + lambda2[i]) / (mi);
                double ro2 = lambda2[i] / (mi);

                double wynik = zad2[i] * (ro2 / ro);
                zad4.Add(wynik);
            }

            Console.WriteLine("Obliczono wspolczynnik straty ruchu tranzytowego.");
        }

        //Zad5
        public void WspolczynnikStratRuchuLokal()
        {
            for (int i = 0; i < 7; i++)
            {
                double ro = (lambda1[i] + lambda2[i]) / (mi);
                double ro1 = lambda1[i] / (mi);

                double wynik = zad3[i] * (ro1 / ro);
                zad5.Add(wynik);
            }

            Console.WriteLine("Obliczono wspolczynnik straty ruchu lokalnego.");
        }

        //Zad6
        public void CalkowitaStrata()
        {
            for(int i = 0; i < 7; i++)
            {
                zad6.Add(zad4[i] + zad5[i]);
            }
            Console.WriteLine("Obliczono calkowitą strate obu strumieni wejsciowych.");
        }

        int decimalpoint = 1000000;

        public void ZrzutDoCsv()
        {
            //zad1
            var csv = new StringBuilder();
            csv.AppendLine("Zgloszenie; Stany systemu");
            for (int i = 0; i < zad1.Count; i++)
            {
                var newLine = string.Format("{0};{1}", i, (Math.Floor(zad1[i] * decimalpoint) / decimalpoint).ToString());
                csv.AppendLine(newLine);
            }
            File.WriteAllText("part1.csv", csv.ToString());
            //zad2
            csv = new StringBuilder();
            csv.AppendLine("Lambda1; Lambda2; prawd. straty tranzyt");
            for (int i = 0; i < lambda1.Count; i++)
            {
                var newLine = string.Format("{0};{1};{2}", lambda1[i], lambda2[i], (Math.Floor(zad2[i] * decimalpoint) / decimalpoint).ToString());
                csv.AppendLine(newLine);
            }
            File.WriteAllText("part2.csv", csv.ToString());
            //zad3
            csv = new StringBuilder();
            csv.AppendLine("Lambda1; Lambda2; praw. straty lokal");
            for (int i = 0; i < lambda1.Count; i++)
            {
                var newLine = string.Format("{0};{1};{2}", lambda1[i], lambda2[i], (Math.Floor(zad3[i] * decimalpoint) / decimalpoint).ToString());
                csv.AppendLine(newLine);
            }
            File.WriteAllText("part3.csv", csv.ToString());
            //zad4
            csv = new StringBuilder();
            csv.AppendLine("Lambda1; Lambda2; wskaz. str. tranz");
            for (int i = 0; i < lambda1.Count; i++)
            {
                var newLine = string.Format("{0};{1};{2}", lambda1[i], lambda2[i], (Math.Floor(zad4[i] * decimalpoint) / decimalpoint).ToString());
                csv.AppendLine(newLine);
            }
            File.WriteAllText("part4.csv", csv.ToString());
            //zad5
            csv = new StringBuilder();
            csv.AppendLine("Lambda1; Lambda2; wskaz.str. lokal");
            for (int i = 0; i < lambda1.Count; i++)
            {
                var newLine = string.Format("{0};{1};{2}", lambda1[i], lambda2[i], (Math.Floor(zad5[i] * decimalpoint) / decimalpoint).ToString());
                csv.AppendLine(newLine);
            }
            File.WriteAllText("part5.csv", csv.ToString());
            //zad6
            csv = new StringBuilder();
            csv.AppendLine("Lambda1; Lambda2; calkowita str.");
            for (int i = 0; i < lambda1.Count; i++)
            {
                var newLine = string.Format("{0};{1};{2}", lambda1[i], lambda2[i], (Math.Floor(zad5[i] * decimalpoint) / decimalpoint).ToString());
                csv.AppendLine(newLine);
            }
            File.WriteAllText("part6.csv", csv.ToString());


            Console.WriteLine("Gotowe! Wyniki w folderze z plikiem.");
            Console.ReadKey();
        }
    }
}
